import sys
import csv
import itertools
import math
import pickle
import time
import datetime

current_timestamp = time.time()
log_start_time = datetime.datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d__%H:%M:%S')
time_start = datetime.datetime.now()

filename = sys.argv[1]
aggfunc = (sys.argv[2]).upper()
group_size = int(sys.argv[3])
dimension = int(filename.split("_")[1])

with open(filename, 'rb') as f:
    reader = csv.reader(f)
    header = next(reader)
    rows = list()
    for row in reader:
        rowInt = list()
        for i in range(len(row)):
            rowInt.append(int(row[i]))
        rows.append(rowInt)
    f.close()

att_size = len(header)
combinations = list(itertools.combinations(rows, group_size))
group_list = list()
max_agg = -sys.maxint

for combination in combinations:
    if aggfunc == 'MAX':
        att = [-sys.maxint] * att_size
    elif aggfunc == 'MIN':
        att = [sys.maxint] * att_size
    elif aggfunc == 'SUM':
        att = [0] * att_size
    else:
        print "aggregate function error"
        sys.exit(0)

    for tuple in combination:
        for i in range(att_size):
            if aggfunc == 'MAX':
                att[i] = max(int(att[i]), int(tuple[i]))
            elif aggfunc == 'MIN':
                att[i] = min(int(att[i]), int(tuple[i]))
            elif aggfunc == 'SUM':
                att[i] = int(att[i]) + int(tuple[i])

    aggregate = list()
    for i in range(att_size):
        aggregate.append(att[i])
        max_agg = max(max_agg, att[i])

    new_group = {
        'group_members' : combination,
        'aggregate' : aggregate
    }

    group_list.append(new_group)

grid = dict()
grid_size = 10
grid_distance = int( math.ceil( float(max_agg)/grid_size ) )

#create grid
# default_cell = {"cell_members" : [],"total" : 0}
if dimension == 2:
    grid = [[{"cell_members" : [],"total" : 0} for i in range(grid_size)] for j in range(grid_size)]
elif dimension == 3:
    grid = [[[{"cell_members" : [],"total" : 0} for i in range(grid_size)] for j in range(grid_size)] for k in range(grid_size)]
elif dimension == 5:
    grid = [[[[[{"cell_members" : [],"total" : 0} for i in range(grid_size)] for j in range(grid_size)] for k in range(grid_size)] for l in range(grid_size)] for m in range(grid_size)]
elif dimension == 7:
    grid = [[[[[[[{"cell_members" : [],"total" : 0} for i in range(grid_size)] for j in range(grid_size)] for k in range(grid_size)] for l in range(grid_size)] for m in range(grid_size)] for n in range(grid_size)] for o in range(grid_size)]
elif dimension == 10:
    grid = [[[[[[[[[[{"cell_members" : [],"total" : 0} for i in range(grid_size)] for j in range(grid_size)] for k in range(grid_size)] for l in range(grid_size)] for m in range(grid_size)] for n in range(grid_size)] for o in range(grid_size)] for p in range(grid_size)] for q in range(grid_size)] for r in range(grid_size)]

for data in group_list:
    position = []
    for aggregate_att in data['aggregate']:
        x = int( math.floor( float(aggregate_att)/grid_distance ) )
        if x == grid_size:
            x -= 1
        position.append( x )
        # print int( math.floor( float(aggregate_att)/grid_distance ) )
    # print "next"
    # print position

    if dimension == 2:
        current_cell = grid[position[0]][position[1]]
    elif dimension == 3:
        current_cell = grid[position[0]][position[1]][position[2]]
    elif dimension == 5:
        current_cell = grid[position[0]][position[1]][position[2]][position[3]][position[4]]
    elif dimension == 7:
        current_cell = grid[position[0]][position[1]][position[2]][position[3]][position[4]][position[5]][position[6]]
    elif dimension == 10:
        current_cell = grid[position[0]][position[1]][position[2]][position[3]][position[4]][position[5]][position[6]][position[7]][position[8]][position[9]]

    current_cell['cell_members'].append(data)
    current_cell['total'] = current_cell['total'] + 1

    if dimension == 2:
        grid[position[0]][position[1]] = current_cell
    elif dimension == 3:
        grid[position[0]][position[1]][position[2]] = current_cell
    elif dimension == 5:
        grid[position[0]][position[1]][position[2]][position[3]][position[4]] = current_cell
    elif dimension == 7:
        grid[position[0]][position[1]][position[2]][position[3]][position[4]][position[5]][position[6]] = current_cell
    elif dimension == 10:
        grid[position[0]][position[1]][position[2]][position[3]][position[4]][position[5]][position[6]][position[7]][position[8]][position[9]] = current_cell

current_timestamp = time.time()
log_end_time = datetime.datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d__%H:%M:%S')
time_end = datetime.datetime.now()

runtime = time_end - time_start

outfilename = aggfunc.lower() + "_" + str(group_size) + "_" + filename.split('.csv')[0] + "_" + str(len(combinations))
with open(outfilename, 'wb') as fo:
    pickle.dump(grid, fo)
    fo.close()

log = list()
log_res = list()
log_res.append(sys.argv)
log_res.append(runtime)
log.append(log_res)

with open("log_initial_step.csv", "a") as output:
    writer = csv.writer(output, lineterminator='\n')
    writer.writerows(log)
    output.close()

print outfilename
