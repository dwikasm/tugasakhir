#!/bin/bash

#grid solution (filename: aggfunc_g_anticorrelated_d_n_comb, k)
#python initial_step.py anticorrelated_3_23.csv min 5

#5 dimensi
python initial_step.py anticorrelated_5_23.csv min 5
python initial_step.py independent_5_23.csv min 5
python initial_step.py forestcover_5_23.csv min 5
echo "initial step 5 dimensi min done"
python grid_solution.py min_5_anticorrelated_5_23_33649 200
python grid_solution.py min_5_independent_5_23_33649 200
python grid_solution.py min_5_forestcover_5_23_33649 200
echo "5 dimensi min done"

#7 dimensi
python initial_step.py anticorrelated_7_23.csv min 5
python initial_step.py independent_7_23.csv min 5
python initial_step.py forestcover_7_23.csv min 5
echo "initial step 7 dimensi min done"
python grid_solution.py min_5_anticorrelated_7_23_33649 200
python grid_solution.py min_5_independent_7_23_33649 200
python grid_solution.py min_5_forestcover_7_23_33649 200
echo "7 dimensi min done"

#10 dimensi
python initial_step.py anticorrelated_10_23.csv min 5
python initial_step.py independent_10_23.csv min 5
python initial_step.py forestcover_10_23.csv min 5
echo "initial step 10 dimensi min done"
python grid_solution.py min_5_anticorrelated_10_23_33649 200
python grid_solution.py min_5_independent_10_23_33649 200
python grid_solution.py min_5_forestcover_10_23_33649 200
echo "10 dimensi min done"




