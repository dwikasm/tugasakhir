<?php

$str = file_get_contents('json_output');
$data = json_decode($str, true); 

// print_r($data);

if (count($data)) {

  // Open the table
  echo "<table>";

  // Cycle through the array
  foreach ($data as $row) {

    // Output a row
  	echo "<tr>";
  	echo "<td>";
  	print_r($row['group_members']);
  	echo "</td>";
  	echo "<td>";
  	print_r($row['aggregate']);
  	echo "</td>";
  	echo "<td>";
  	print_r($row['score']);
  	echo "</td>";
  	echo "</tr>";

  }

  // Close the table
  echo "</table>";
}

?>