// Send Jquery AJAX request
function send_ajax_request(method, url, data){

  // Set default value
  if (data === undefined) {
    data = null
  }

  // Prepare ajax settings
  var settings = {
    "async": true,
    "url": url,
    "method": method,
    "processData": false,
    "contentType": false,
    "mimeType": "multipart/form-data",
    "data": data,
    error: function (xhr, responseData, textStatus) {
      console.log(xhr.status+" ERROR: "+textStatus);
    }
  }
  return $.ajax(settings)
}

function submit_query(form_data){
  var form = new FormData();
  var data = form_data.serializeArray();
  $.each(data, function(key, input) {
    form.append(input.name, input.value);
  });
  form.append('dataset', $('#dataset')[0].files[0]);

  var solution = send_ajax_request('POST', 'api/api.php', form);
  $("#overlay").show();
  $.when(solution).done(function(responseData, textStatus){
    console.log(responseData);
    $("#result_div").html("<div class='card'><div id='result_header' class='card-header'></div><div id='result_body' class='card-body'></div></div>");
    $("#result_header").html("<p>Waktu eksekusi : <b>"+responseData+"</b></p>")
    $.getJSON("json_output.json", function(result){
      console.log("masuk getjson");
      console.log(result);
      $("#result_body").html("<table class='table table-striped table-bordered'><thead class='thead-dark'><tr><th scope='col'>#</th><th scope='col'>Anggota Kelompok</th><th scope='col'>Nilai Agregat</th><th scope='col'>Skor</th></tr></thead><tbody id='table_body'></tbody></table>");
      $.each(result, function(i, field){
          $("#table_body").append("<tr><th scope='row'>"+(i+1)+"</th><td>"+field['group_members'].join(" | ")+"</td><td>"+field['aggregate']+"</td><td>"+field['score']+"</td></tr>");   
      });
    });
    $("#overlay").hide();
  });
}