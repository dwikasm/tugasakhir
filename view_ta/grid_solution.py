import sys
import pickle
import time
import datetime
import itertools
import os
import json
import csv

try:
    import psutil
    psutil_flag = True
except ImportError:
    psutil_flag = False


def keyStringToList(my_string):
    return map(int, my_string.split(','))


def keyListToString(my_list):
    return ','.join([str(x) for x in my_list])


def candidateCheck(curpos_string):
    global grid
    global cell_cand_list
    global cell_not_cant_list
    global k

    curpos = keyStringToList(curpos_string)

    list_of_list = list()
    for x in curpos:
        lis = list()
        for y in range(1, x+1):
            lis.append(y)
        list_of_list.append(lis)

    products_list = list(itertools.product(*list_of_list))
    better_cell_list = list()
    partial_cell_list = list()
    maximum_size = 0
    candidate_flag = True

    for product in products_list:
        lproduct = list(product)
        att_ditt = 0
        for i in range(len(lproduct)):
            if lproduct[i] != curpos[i]:
                att_ditt += 1

        if att_ditt == len(lproduct):
            better_cell_list.append(lproduct)
        elif att_ditt > 0:
            partial_cell_list.append(lproduct)

    if candidate_flag:
        for cell in better_cell_list:
            cell_key = keyListToString(cell)
            if cell_key in grid.keys():
                curcell = grid[cell_key]
                if curcell in cell_not_cant_list:
                    candidate_flag = False
                    break
                maximum_size += curcell['total']
            if maximum_size > k:
                candidate_flag = False
                break

    if candidate_flag:
        cell_cand_list.append(grid[curpos_string])
    else:
        cell_not_cant_list.append(grid[curpos_string])

    return

current_timestamp = time.time()
log_start_time = datetime.datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d__%H:%M:%S')
time_start = datetime.datetime.now()

filename = sys.argv[1]
k = int(sys.argv[2])

with open(filename, 'rb') as f:
    grid = pickle.load(f)
    f.close()

cell_cand_list = list()
cell_not_cant_list = list()

for key in grid.keys():
    candidateCheck(key)

final_groups = list()

for cell in cell_cand_list:
    for group in cell['cell_members']:
        final_groups.append(group)

for group in final_groups:
    domination = 0
    for other_group in final_groups:
        if group == other_group:
            continue
        else:
            agg1 = group['aggregate']
            agg2 = other_group['aggregate']
            att_domination = False
            for i in range(len(agg1)):
                if agg1[i] < agg2[i]:
                    att_domination = True
                elif agg1[i] > agg2[i]:
                    att_domination = False
                    break
            if att_domination:
                domination += 1
    group['score'] = domination


final_groups.sort(key=lambda x: x['score'], reverse=True)

if k > len(final_groups):
    outsize = len(final_groups)
else:
    outsize = k

final_rank = list()
for i in range(outsize):
    final_rank.append(final_groups[i])

time_end = datetime.datetime.now()

current_timestamp = time.time()
log_end_time = datetime.datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d__%H:%M:%S')

runtime = time_end - time_start

if psutil_flag:
    process = psutil.Process(os.getpid())
    mem_usage = float(process.memory_info().rss)/1000000.0
else:
    mem_usage = "-"

log = list()
log_data = list()

split = filename.split('_')
aggregate_func = split[0]
group_size = split[1]
data_type = split[2]
dimension = split[3]
n_size = split[4]
combination_size = split[5]

log_data.append(log_start_time)
log_data.append(log_end_time)
log_data.append("offered algo ver3")
log_data.append(data_type)
log_data.append(dimension)
log_data.append(n_size)
log_data.append(combination_size)
log_data.append(k)
log_data.append(group_size)
log_data.append(aggregate_func)
log_data.append(runtime)
log_data.append(mem_usage)

log.append(log_data)

print runtime

list_of_dict = final_rank

with open('json_output.json', 'w') as fout:
    json.dump(list_of_dict, fout)
    fout.close()

with open("log.csv", "a") as output:
    writer = csv.writer(output, lineterminator='\n')
    writer.writerows(log)
    output.close()