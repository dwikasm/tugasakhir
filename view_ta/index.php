<!DOCTYPE html>
<html lang="en">
<head>
  <title>TA View</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
</head>
<body>

<div class="container">
  <div id="overlay"></div>
  <div class="row">
    <div class="col-sm-12" style="margin-top: 1em">
      <div class="card">
        <div class="card-header">
          <h5 class="card-title">Desain dan Implementasi Pengolahan Top-K Dominating Query Pada Data Berbasis Kelompok</h5>
          <p class="card-text">Dwika Setya Muhammad (051114000142)</p>
        </div>
        <div class="card-body">
          
          <form id="query_form">
            <div class="form-group">
              <label for="dataset">Upload Dataset</label>
              <input type="file" class="form-control-file" id="dataset" name="dataset">
            </div>

            <div class="form-group">
              <label for="aggfunc">Fungsi Agregat</label>
              <select class="form-control" id="aggfunc" name="aggfunc">
                <option>MAX</option>
                <option>MIN</option>
                <option>SUM</option>
              </select>
            </div>

            <div class="form-group">
              <label for="group_size">Ukuran Kelompok</label>
              <select class="form-control" id="group_size" name="group_size">
                <option>3</option>
                <option>5</option>
                <option>7</option>
                <option>10</option>
              </select>
            </div>

            <div class="form-group">
              <label for="k_size">Ukuran K</label>
              <input type="text" class="form-control" id="k_size" name="k_size">
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>

          </form>

        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div id="result_div" class="col-sm-12" style="margin-top: 1em">
    </div>
  </div>

</div>  

<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/main.js"></script>

</body>
</html>