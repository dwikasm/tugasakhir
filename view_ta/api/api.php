<?php
$uploaddir = '../';
$file_name = NULL;
$file = $_FILES['dataset'];

if ($file_name == NULL) {
  // $file_name = underscore($file['name']);
  $file_name = $file['name'];
}
else {
  $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
  $file_name = $file_name.'.'.$ext;
}
$uploadfile = $uploaddir.$file_name;

if (move_uploaded_file($file['tmp_name'], $uploadfile)) {
  // echo "FILE UPLOADED"; echo "\n";
  chdir("../");
	
  $command1 = "python initial_step.py ".$file['name']." ".$_POST['aggfunc']." ".$_POST['group_size'];
  // echo $command1."\n"; 
  $output1 = shell_exec($command1);
  $output1 = trim(preg_replace('/\s\s+/', ' ', $output1));
  
  $command2 = "python grid_solution.py ".$output1." ".$_POST['k_size'];
  // echo $command2."\n";
  $output2 = shell_exec($command2);
  echo $output2;
}
else {
  echo "FAILED TO UPLOAD";
}
