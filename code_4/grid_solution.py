import sys
import pickle
import time
import datetime
import itertools
import os
import json
import csv

try:
    import psutil
    psutil_flag = True
except ImportError:
    psutil_flag = False

def keyStringToList(my_string):
    return map(int, my_string.split(','))


def keyListToString(my_list):
    return ','.join([ str(x) for x in my_list ])

def candidateCheck(curpos_string, dimension):
    global grid
    global cell_cand_list
    global cell_not_cand_list
    global k

    curpos = keyStringToList(curpos_string)
    candidate_flag = True
    number_of_dominator = 0

    class BreakNested(Exception):
        pass

    if dimension == '2':
        try:
            for i in range(curpos[0] - 1, 0, -1):
                for j in range(curpos[1] - 1, 0, -1):
                    current_cell_key = [i, j]
                    current_cell_key_s = keyListToString(current_cell_key)
                    if current_cell_key_s in grid.keys():
                        current_cell = grid[current_cell_key_s]
                        # sel bukan kandidat jika sel yang mendominasi dia adalah anggota sel bukan kandidat
                        if current_cell in cell_not_cand_list:
                            candidate_flag = False
                            raise BreakNested
                        else:
                            number_of_dominator += current_cell['total']
                    # sel bukan kandidat jika jumlah yang mendominasi dia lebih dari k
                    if number_of_dominator > k:
                        candidate_flag = False
                        raise BreakNested
        except BreakNested:
            pass

        if candidate_flag:
            cell_cand_list.append(grid[curpos_string])
        else:
            cell_not_cand_list.append(grid[curpos_string])
    
    elif dimension == '3':
        try:
            for i in range(curpos[0] - 1, 0, -1):
                for j in range(curpos[1] - 1, 0, -1):
                    for k in range(curpos[2] - 1, 0, -1):
                        current_cell_key = [i, j, k]
                        current_cell_key_s = keyListToString(current_cell_key)
                        if current_cell_key_s in grid.keys():
                            current_cell = grid[current_cell_key_s]
                            # sel bukan kandidat jika sel yang mendominasi dia adalah anggota sel bukan kandidat
                            if current_cell in cell_not_cand_list:
                                candidate_flag = False
                                raise BreakNested
                            else:
                                number_of_dominator += current_cell['total']
                        # sel bukan kandidat jika jumlah yang mendominasi dia lebih dari k
                        if number_of_dominator > k:
                            candidate_flag = False
                            raise BreakNested
        except BreakNested:
            pass

        if candidate_flag:
            cell_cand_list.append(grid[curpos_string])
        else:
            cell_not_cand_list.append(grid[curpos_string])
    
    elif dimension == '5':
        try:
            for i in range(curpos[0] - 1, 0, -1):
                for j in range(curpos[1] - 1, 0, -1):
                    for k in range(curpos[2] - 1, 0, -1):
                        for l in range(curpos[3] - 1, 0, -1):
                            for m in range(curpos[4] - 1, 0, -1):
                                current_cell_key = [i, j, k, l, m]
                                current_cell_key_s = keyListToString(current_cell_key)
                                if current_cell_key_s in grid.keys():
                                    current_cell = grid[current_cell_key_s]
                                    # sel bukan kandidat jika sel yang mendominasi dia adalah anggota sel bukan kandidat
                                    if current_cell in cell_not_cand_list:
                                        candidate_flag = False
                                        raise BreakNested
                                    else:
                                        number_of_dominator += current_cell['total']
                                # sel bukan kandidat jika jumlah yang mendominasi dia lebih dari k
                                if number_of_dominator > k:
                                    candidate_flag = False
                                    raise BreakNested
        except BreakNested:
            pass

        if candidate_flag:
            cell_cand_list.append(grid[curpos_string])
        else:
            cell_not_cand_list.append(grid[curpos_string])
    
    elif dimension == '7':
        try:
            for i in range(curpos[0] - 1, 0, -1):
                for j in range(curpos[1] - 1, 0, -1):
                    for k in range(curpos[2] - 1, 0, -1):
                        for l in range(curpos[3] - 1, 0, -1):
                            for m in range(curpos[4] - 1, 0, -1):
                                for n in range(curpos[5] - 1, 0, -1):
                                    for o in range(curpos[6] - 1, 0, -1):
                                        current_cell_key = [i, j, k, l, m, n, o]
                                        current_cell_key_s = keyListToString(current_cell_key)
                                        if current_cell_key_s in grid.keys():
                                            current_cell = grid[current_cell_key_s]
                                            # sel bukan kandidat jika sel yang mendominasi dia adalah anggota sel bukan kandidat
                                            if current_cell in cell_not_cand_list:
                                                candidate_flag = False
                                                raise BreakNested
                                            else:
                                                number_of_dominator += current_cell['total']
                                        #sel bukan kandidat jika jumlah yang mendominasi dia lebih dari k
                                        if number_of_dominator > k:
                                            candidate_flag = False
                                            raise BreakNested
        except BreakNested:
            pass
        
        if candidate_flag:
            cell_cand_list.append(grid[curpos_string])
        else:
            cell_not_cand_list.append(grid[curpos_string])
            
    elif dimension == '10':
        try:
            for i in range(curpos[0] - 1, 0, -1):
                for j in range(curpos[1] - 1, 0, -1):
                    for k in range(curpos[2] - 1, 0, -1):
                        for l in range(curpos[3] - 1, 0, -1):
                            for m in range(curpos[4] - 1, 0, -1):
                                for n in range(curpos[5] - 1, 0, -1):
                                    for o in range(curpos[6] - 1, 0, -1):
                                        for p in range(curpos[4] - 1, 0, -1):
                                            for q in range(curpos[5] - 1, 0, -1):
                                                for r in range(curpos[6] - 1, 0, -1):
                                                    current_cell_key = [i, j, k, l, m, n, o, p, q, r]
                                                    current_cell_key_s = keyListToString(current_cell_key)
                                                    if current_cell_key_s in grid.keys():
                                                        current_cell = grid[current_cell_key_s]
                                                        # sel bukan kandidat jika sel yang mendominasi dia adalah anggota sel bukan kandidat
                                                        if current_cell in cell_not_cand_list:
                                                            candidate_flag = False
                                                            raise BreakNested
                                                        else:
                                                            number_of_dominator += current_cell['total']
                                                    # sel bukan kandidat jika jumlah yang mendominasi dia lebih dari k
                                                    if number_of_dominator > k:
                                                        candidate_flag = False
                                                        raise BreakNested
        except BreakNested:
            pass

        if candidate_flag:
            cell_cand_list.append(grid[curpos_string])
        else:
            cell_not_cand_list.append(grid[curpos_string])

    return

current_timestamp = time.time()
log_start_time = datetime.datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d__%H:%M:%S')
time_start = datetime.datetime.now()

filename = sys.argv[1]
k = int(sys.argv[2])
dimension = filename.split("_")[3]

with open(filename, 'rb') as f:
    grid = pickle.load(f)
    f.close()

time1 = datetime.datetime.now()

cell_cand_list = list()
cell_not_cand_list = list()

for key in grid.keys():
    candidateCheck(key,dimension)

time2 = datetime.datetime.now()

final_groups = list()

for cell in cell_cand_list:
    for group in cell['cell_members']:
        final_groups.append(group)

time3 = datetime.datetime.now()
print "all group candidate", len(final_groups)

komparasi = 0
for group in final_groups:
    domination = 0
    for other_group in final_groups:
        komparasi += 1
        if group == other_group:
            continue
        else:
            agg1 = group['aggregate']
            agg2 = other_group['aggregate']
            att_domination = False
            for i in range(len(agg1)):
                if agg1[i] < agg2[i]:
                    att_domination = True
                elif agg1[i] > agg2[i]:
                    att_domination = False
                    break
            if att_domination:
                domination += 1
    group['score'] = domination

print "komparasi =", komparasi
time4 = datetime.datetime.now()

final_groups.sort(key=lambda x: x['score'], reverse=True)

if k > len(final_groups):
    outsize = len(final_groups)
else:
    outsize = k

final_rank = list()
for i in range(outsize):
    final_rank.append(final_groups[i])

time_end = datetime.datetime.now()

current_timestamp = time.time()
log_end_time = datetime.datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d__%H:%M:%S')

runtime = time_end - time_start

if psutil_flag:
    process = psutil.Process(os.getpid())
    mem_usage = float(process.memory_info().rss)/1000000.0
else:
    mem_usage = "-"

print "Candidate check =", time2-time1
print "Append all group candidate =", time3-time2
print "Group domination check =", time4-time3

log = list()
log_data = list()

split = filename.split('_')
aggregate_func = split[0]
group_size = split[1]
data_type = split[2]
# dimension = split[3]
n_size = split[4]
combination_size = split[5]

log_data.append(log_start_time)
log_data.append(log_end_time)
log_data.append("offered algo ver4")
log_data.append(data_type)
log_data.append(dimension)
log_data.append(n_size)
log_data.append(combination_size)
log_data.append(k)
log_data.append(group_size)
log_data.append(aggregate_func)
log_data.append(runtime)
log_data.append(mem_usage)

log.append(log_data)

# python grid_solution.py max_5_anticorrelated_3_19_11628 200
print "runtime :", runtime
# sys.exit()
# for x in final_rank:
#     print x.group_members,":",x.aggregate
# sys.exit()

list_of_dict = final_rank
# print output_json

with open('json_output.json', 'w') as fout:
   json.dump(list_of_dict, fout)
   fout.close()

with open("log.csv", "a") as output:
    writer = csv.writer(output, lineterminator='\n')
    writer.writerows(log)
    output.close()
