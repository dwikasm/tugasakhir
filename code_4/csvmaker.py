import csv

filename = "log.csv"

rows = list()
data_title = list()
data_title.append("time_start")
data_title.append("time_end")
data_title.append("algorithm")
data_title.append("data_type")
data_title.append("dimension")
data_title.append("n_size")
data_title.append("combination_size")
data_title.append("k")
data_title.append("group_size")
data_title.append("aggregate_func")
data_title.append("runtime")
data_title.append("mem_usage")
rows.append(data_title)

with open(filename, 'wb') as csvfile:
    csvwriter = csv.writer(csvfile)
    for row in rows:
        csvwriter.writerow(row)