#!/bin/bash

#brute solution (filename: anticorrelated_d_n.csv, k, groupsize, aggfunc)

python brute_solution.py anticorrelated_2_23.csv 200 5 max
python brute_solution.py anticorrelated_3_23.csv 200 5 max
python brute_solution.py anticorrelated_5_23.csv 200 5 max
python brute_solution.py anticorrelated_7_23.csv 200 5 max
python brute_solution.py anticorrelated_10_23.csv 200 5 max

python brute_solution.py anticorrelated_3_19.csv 200 5 max
python brute_solution.py anticorrelated_3_23.csv 200 5 max
python brute_solution.py anticorrelated_3_25.csv 200 5 max
python brute_solution.py anticorrelated_3_29.csv 200 5 max
python brute_solution.py anticorrelated_3_32.csv 200 5 max

python brute_solution.py anticorrelated_3_23.csv 100 5 max
python brute_solution.py anticorrelated_3_23.csv 200 5 max
python brute_solution.py anticorrelated_3_23.csv 500 5 max
python brute_solution.py anticorrelated_3_23.csv 1000 5 max

python brute_solution.py anticorrelated_3_58.csv 200 3 max
python brute_solution.py anticorrelated_3_23.csv 200 5 max
python brute_solution.py anticorrelated_3_18.csv 200 7 max
python brute_solution.py anticorrelated_3_18.csv 200 10 max

python brute_solution.py independent_2_23.csv 200 5 max
python brute_solution.py independent_3_23.csv 200 5 max
python brute_solution.py independent_5_23.csv 200 5 max
python brute_solution.py independent_7_23.csv 200 5 max
python brute_solution.py independent_10_23.csv 200 5 max

python brute_solution.py independent_3_19.csv 200 5 max
python brute_solution.py independent_3_23.csv 200 5 max
python brute_solution.py independent_3_25.csv 200 5 max
python brute_solution.py independent_3_29.csv 200 5 max
python brute_solution.py independent_3_32.csv 200 5 max

python brute_solution.py independent_3_23.csv 100 5 max
python brute_solution.py independent_3_23.csv 200 5 max
python brute_solution.py independent_3_23.csv 500 5 max
python brute_solution.py independent_3_23.csv 1000 5 max

python brute_solution.py independent_3_58.csv 200 3 max
python brute_solution.py independent_3_23.csv 200 5 max
python brute_solution.py independent_3_18.csv 200 7 max
python brute_solution.py independent_3_18.csv 200 10 max

python brute_solution.py forestcover_2_23.csv 200 5 max
python brute_solution.py forestcover_3_23.csv 200 5 max
python brute_solution.py forestcover_5_23.csv 200 5 max
python brute_solution.py forestcover_7_23.csv 200 5 max
python brute_solution.py forestcover_10_23.csv 200 5 max

python brute_solution.py forestcover_3_19.csv 200 5 max
python brute_solution.py forestcover_3_23.csv 200 5 max
python brute_solution.py forestcover_3_25.csv 200 5 max
python brute_solution.py forestcover_3_29.csv 200 5 max
python brute_solution.py forestcover_3_32.csv 200 5 max

python brute_solution.py forestcover_3_23.csv 100 5 max
python brute_solution.py forestcover_3_23.csv 200 5 max
python brute_solution.py forestcover_3_23.csv 500 5 max
python brute_solution.py forestcover_3_23.csv 1000 5 max

python brute_solution.py forestcover_3_58.csv 200 3 max
python brute_solution.py forestcover_3_23.csv 200 5 max
python brute_solution.py forestcover_3_18.csv 200 7 max
python brute_solution.py forestcover_3_18.csv 200 10 max

python brute_solution.py anticorrelated_2_23.csv 200 5 min
python brute_solution.py anticorrelated_3_23.csv 200 5 min
python brute_solution.py anticorrelated_5_23.csv 200 5 min
python brute_solution.py anticorrelated_7_23.csv 200 5 min
python brute_solution.py anticorrelated_10_23.csv 200 5 min

python brute_solution.py anticorrelated_3_19.csv 200 5 min
python brute_solution.py anticorrelated_3_23.csv 200 5 min
python brute_solution.py anticorrelated_3_25.csv 200 5 min
python brute_solution.py anticorrelated_3_29.csv 200 5 min
python brute_solution.py anticorrelated_3_32.csv 200 5 min

python brute_solution.py anticorrelated_3_23.csv 100 5 min
python brute_solution.py anticorrelated_3_23.csv 200 5 min
python brute_solution.py anticorrelated_3_23.csv 500 5 min
python brute_solution.py anticorrelated_3_23.csv 1000 5 min

python brute_solution.py anticorrelated_3_58.csv 200 3 min
python brute_solution.py anticorrelated_3_23.csv 200 5 min
python brute_solution.py anticorrelated_3_18.csv 200 7 min
python brute_solution.py anticorrelated_3_18.csv 200 10 min

python brute_solution.py independent_2_23.csv 200 5 min
python brute_solution.py independent_3_23.csv 200 5 min
python brute_solution.py independent_5_23.csv 200 5 min
python brute_solution.py independent_7_23.csv 200 5 min
python brute_solution.py independent_10_23.csv 200 5 min

python brute_solution.py independent_3_19.csv 200 5 min
python brute_solution.py independent_3_23.csv 200 5 min
python brute_solution.py independent_3_25.csv 200 5 min
python brute_solution.py independent_3_29.csv 200 5 min
python brute_solution.py independent_3_32.csv 200 5 min

python brute_solution.py independent_3_23.csv 100 5 min
python brute_solution.py independent_3_23.csv 200 5 min
python brute_solution.py independent_3_23.csv 500 5 min
python brute_solution.py independent_3_23.csv 1000 5 min

python brute_solution.py independent_3_58.csv 200 3 min
python brute_solution.py independent_3_23.csv 200 5 min
python brute_solution.py independent_3_18.csv 200 7 min
python brute_solution.py independent_3_18.csv 200 10 min

python brute_solution.py forestcover_2_23.csv 200 5 min
python brute_solution.py forestcover_3_23.csv 200 5 min
python brute_solution.py forestcover_5_23.csv 200 5 min
python brute_solution.py forestcover_7_23.csv 200 5 min
python brute_solution.py forestcover_10_23.csv 200 5 min

python brute_solution.py forestcover_3_19.csv 200 5 min
python brute_solution.py forestcover_3_23.csv 200 5 min
python brute_solution.py forestcover_3_25.csv 200 5 min
python brute_solution.py forestcover_3_29.csv 200 5 min
python brute_solution.py forestcover_3_32.csv 200 5 min

python brute_solution.py forestcover_3_23.csv 100 5 min
python brute_solution.py forestcover_3_23.csv 200 5 min
python brute_solution.py forestcover_3_23.csv 500 5 min
python brute_solution.py forestcover_3_23.csv 1000 5 min

python brute_solution.py forestcover_3_58.csv 200 3 min
python brute_solution.py forestcover_3_23.csv 200 5 min
python brute_solution.py forestcover_3_18.csv 200 7 min
python brute_solution.py forestcover_3_18.csv 200 10 min

python brute_solution.py anticorrelated_2_23.csv 200 5 sum
python brute_solution.py anticorrelated_3_23.csv 200 5 sum
python brute_solution.py anticorrelated_5_23.csv 200 5 sum
python brute_solution.py anticorrelated_7_23.csv 200 5 sum
python brute_solution.py anticorrelated_10_23.csv 200 5 sum

python brute_solution.py anticorrelated_3_19.csv 200 5 sum
python brute_solution.py anticorrelated_3_23.csv 200 5 sum
python brute_solution.py anticorrelated_3_25.csv 200 5 sum
python brute_solution.py anticorrelated_3_29.csv 200 5 sum
python brute_solution.py anticorrelated_3_32.csv 200 5 sum

python brute_solution.py anticorrelated_3_23.csv 100 5 sum
python brute_solution.py anticorrelated_3_23.csv 200 5 sum
python brute_solution.py anticorrelated_3_23.csv 500 5 sum
python brute_solution.py anticorrelated_3_23.csv 1000 5 sum

python brute_solution.py anticorrelated_3_58.csv 200 3 sum
python brute_solution.py anticorrelated_3_23.csv 200 5 sum
python brute_solution.py anticorrelated_3_18.csv 200 7 sum
python brute_solution.py anticorrelated_3_18.csv 200 10 sum

python brute_solution.py independent_2_23.csv 200 5 sum
python brute_solution.py independent_3_23.csv 200 5 sum
python brute_solution.py independent_5_23.csv 200 5 sum
python brute_solution.py independent_7_23.csv 200 5 sum
python brute_solution.py independent_10_23.csv 200 5 sum

python brute_solution.py independent_3_19.csv 200 5 sum
python brute_solution.py independent_3_23.csv 200 5 sum
python brute_solution.py independent_3_25.csv 200 5 sum
python brute_solution.py independent_3_29.csv 200 5 sum
python brute_solution.py independent_3_32.csv 200 5 sum

python brute_solution.py independent_3_23.csv 100 5 sum
python brute_solution.py independent_3_23.csv 200 5 sum
python brute_solution.py independent_3_23.csv 500 5 sum
python brute_solution.py independent_3_23.csv 1000 5 sum

python brute_solution.py independent_3_58.csv 200 3 sum
python brute_solution.py independent_3_23.csv 200 5 sum
python brute_solution.py independent_3_18.csv 200 7 sum
python brute_solution.py independent_3_18.csv 200 10 sum

python brute_solution.py forestcover_2_23.csv 200 5 sum
python brute_solution.py forestcover_3_23.csv 200 5 sum
python brute_solution.py forestcover_5_23.csv 200 5 sum
python brute_solution.py forestcover_7_23.csv 200 5 sum
python brute_solution.py forestcover_10_23.csv 200 5 sum

python brute_solution.py forestcover_3_19.csv 200 5 sum
python brute_solution.py forestcover_3_23.csv 200 5 sum
python brute_solution.py forestcover_3_25.csv 200 5 sum
python brute_solution.py forestcover_3_29.csv 200 5 sum
python brute_solution.py forestcover_3_32.csv 200 5 sum

python brute_solution.py forestcover_3_23.csv 100 5 sum
python brute_solution.py forestcover_3_23.csv 200 5 sum
python brute_solution.py forestcover_3_23.csv 500 5 sum
python brute_solution.py forestcover_3_23.csv 1000 5 sum

python brute_solution.py forestcover_3_58.csv 200 3 sum
python brute_solution.py forestcover_3_23.csv 200 5 sum
python brute_solution.py forestcover_3_18.csv 200 7 sum
python brute_solution.py forestcover_3_18.csv 200 10 sum

echo "script brute force done"