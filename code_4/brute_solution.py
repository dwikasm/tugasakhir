import csv
import sys
import psutil
import os
import time
import datetime

current_timestamp = time.time()
log_start_time = datetime.datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d__%H:%M:%S')
time_start = datetime.datetime.now()

filename = sys.argv[1]
k = int(sys.argv[2])
gsize = int(sys.argv[3])
aggfunc = sys.argv[4]

# Read csv data
with open(filename, 'rb') as f:
    reader = csv.reader(f)
    # discard header
    header = next(reader)
    dataLength = len(header)
    # initialize list
    rows = list()
    # append data
    for row in reader:
        rowInt = list()
        for i in range(0, len(row)):
            rowInt.append(int(row[i]))
        rows.append((rowInt))
    f.close()

# print header
# for row in rows:
#     print row

aggfunc = aggfunc.upper()
# while (aggfunc != 'MAX' and aggfunc != 'MIN'):
#     aggfunc = raw_input("Aggregate function used (min/max): ")
#     aggfunc = aggfunc.upper()

# print "working.."

import time
start_time = time.time()

import itertools
combinations = list(itertools.combinations(rows, gsize))

attsize = len(header)

# print "\nkombinasi data:"
newRows = list()
for row in combinations:
    if aggfunc == 'MAX':
        att = [-sys.maxint] * attsize
    elif aggfunc == 'MIN':
        att = [sys.maxint] * attsize
    elif aggfunc == 'SUM':
        att = [0] * attsize
    for tuple in row:
        for i in range(0, attsize):
            if aggfunc == 'MAX':
                att[i] = max(att[i], tuple[i])
            elif aggfunc == 'MIN':
                att[i] = min(att[i], tuple[i])
            elif aggfunc == 'SUM':
                att[i] = att[i] + tuple[i]
    agregat = list()
    for i in range(0, len(att)):
        agregat.append((att[i]))
    # print agregat
    newRow = list()
    newRow.append(row)
    newRow.append(agregat)
    newRows.append(newRow)
    # print row

# print "\ndata dengan nilai agregat:"
# print len(newRows)
# sys.exit()
komparasi = 0
for row in newRows:
    # print row[1]
    domination = 0
    for innerRow in newRows:
        komparasi += 1
        if row == innerRow:
            continue
        else:
            # cek mendominasi atau tidak
            agg1 = row[1]
            agg2 = innerRow[1]
            attDomination = False
            for i in range(0,attsize):
                if agg1[i] < agg2[i]:
                    attDomination = True
                elif agg1[i] > agg2[i]:
                    attDomination = False
                    break
            if attDomination:
                domination += 1
    row.append(domination)
print "komparasi =", komparasi
newRows.sort(key=lambda x: x[2], reverse=True)
# output = sorted(newRows, key=lambda x: x[2])

if k > len(newRows):
    outsize = len(newRows)
    # print "K exceeds number of combinations. Displaying %i instead." % (outsize)
else:
    outsize = k

# print filename
# print "Top-%i groups with brute force:" % outsize
# for i in range(0,outsize):
    # print (newRows[i])
    # print "%i.\tGroup members\t\t\t: %s\n" \
    #       "\tAggregate function(%s)\t: %s\n" \
    #       "\tDomination Score\t\t: %i" % (i+1, str((newRows[i])[0]), aggfunc, str((newRows[i])[1]), (newRows[i])[2])
    # print (output[i])

# print("Total runtime : %s seconds " % (time.time() - start_time))

time_end = datetime.datetime.now()
# total_time = time.time() - start_time
# print "Total runtime : %s seconds" % total_time

current_timestamp = time.time()
log_end_time = datetime.datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d__%H:%M:%S')

runtime = time_end - time_start

process = psutil.Process(os.getpid())
mem_usage = float(process.memory_info().rss)/1000000.0

log = list()
log_data = list()

split = filename.split('_')
aggregate_func = aggfunc
group_size = gsize
data_type = split[0]
dimension = split[1]
n_size = split[2]
n_size = n_size.split('.csv')[0]
combination_size = len(combinations)

log_data.append(log_start_time)
log_data.append(log_end_time)
log_data.append("brute force")
log_data.append(data_type)
log_data.append(dimension)
log_data.append(n_size)
log_data.append(combination_size)
log_data.append(k)
log_data.append(group_size)
log_data.append(aggregate_func.lower())
log_data.append(runtime)
log_data.append(mem_usage)

log.append(log_data)

# python brute_solution.py anticorrelated_3_19.csv 200 5 max
print "runtime :",runtime
sys.exit()

with open("log.csv", "a") as output:
    writer = csv.writer(output, lineterminator='\n')
    writer.writerows(log)
    output.close()

for i in range(outsize):
    print newRows[i][0],":",newRows[i][1]