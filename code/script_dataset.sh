#!/bin/bash

python dataset_creator.py anticorrelated 2 23
python dataset_creator.py anticorrelated 3 23
python dataset_creator.py anticorrelated 5 23
python dataset_creator.py anticorrelated 7 23
python dataset_creator.py anticorrelated 10 23

python dataset_creator.py anticorrelated 3 19
python dataset_creator.py anticorrelated 3 23
python dataset_creator.py anticorrelated 3 25
python dataset_creator.py anticorrelated 3 29
python dataset_creator.py anticorrelated 3 32

python dataset_creator.py anticorrelated 3 58
python dataset_creator.py anticorrelated 3 23
python dataset_creator.py anticorrelated 3 18
python dataset_creator.py anticorrelated 3 18

python dataset_creator.py independent 2 23
python dataset_creator.py independent 3 23
python dataset_creator.py independent 5 23
python dataset_creator.py independent 7 23
python dataset_creator.py independent 10 23

python dataset_creator.py independent 3 19
python dataset_creator.py independent 3 23
python dataset_creator.py independent 3 25
python dataset_creator.py independent 3 29
python dataset_creator.py independent 3 32

python dataset_creator.py independent 3 58
python dataset_creator.py independent 3 23
python dataset_creator.py independent 3 18
python dataset_creator.py independent 3 18

python dataset_creator.py forestcover 2 23
python dataset_creator.py forestcover 3 23
python dataset_creator.py forestcover 5 23
python dataset_creator.py forestcover 7 23
python dataset_creator.py forestcover 10 23

python dataset_creator.py forestcover 3 19
python dataset_creator.py forestcover 3 23
python dataset_creator.py forestcover 3 25
python dataset_creator.py forestcover 3 29
python dataset_creator.py forestcover 3 32

python dataset_creator.py forestcover 3 58
python dataset_creator.py forestcover 3 23
python dataset_creator.py forestcover 3 18
python dataset_creator.py forestcover 3 18

echo "script dataset done"