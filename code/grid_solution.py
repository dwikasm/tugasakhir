import sys
import pickle
import time
import datetime
import itertools
import os
import psutil
import csv

class Group:
    def __init__(self):
        self.score = 0
        self.group_members = []
        self.aggregate = []

class GridCell:
    def __init__(self):
        self.total = 0
        self.cell_members = []
        self.position = ""

    def add_cell_member(self, group):
        self.cell_members.append(group)
        self.total += 1

def keyStringToList(my_string):
    return map(int, my_string.split(','))

def keyListToString(my_list):
    return ','.join( [ str(x) for x in my_list ] )

def candidateCheck(curpos_string):
    global grid
    global cell_cand
    global cell_not_cand
    global k

    curpos = keyStringToList(curpos_string)

    list_of_list = list()
    for x in curpos:
        lis = list()
        for y in range(1, x+1):
            lis.append(y)
        list_of_list.append(lis)

    products = list(itertools.product(*list_of_list))
    better_cell = list()
    partial_cell = list()
    maximum_size = 0
    for product in products:
        lproduct = list(product)
        att_diff =0
        for i in range(len(lproduct)):
            if lproduct[i] != curpos[i]:
                att_diff += 1

        if att_diff > 1:
            better_cell.append(lproduct)
        else:
            partial_cell.append(lproduct)

    # for cell in partial_cell:
    #     cell_key = keyListToString(cell)
    #     if cell_key in grid.keys():
    #         if grid[cell_key] in cell_not_cand:
    #             cell_not_cand.append(grid[curpos_string])
    #             return

    for cell in better_cell:
        cell_key = keyListToString(cell)
        if cell_key in grid.keys():
            if grid[cell_key] in cell_not_cand:
                cell_not_cand.append(grid[curpos_string])
                return
            maximum_size += grid[cell_key].total
        if maximum_size > k:
            break

    if maximum_size > k:
        cell_not_cand.append(grid[curpos_string])
    else:
        cell_cand.append(grid[curpos_string])
    return

current_timestamp = time.time()
log_start_time = datetime.datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d__%H:%M:%S')
time_start = datetime.datetime.now()

filename = sys.argv[1]
k = sys.argv[2]

with open(filename, 'rb') as f:
    grid = pickle.load(f)
    f.close()

cell_cand = list()
cell_not_cand = list()

keys = grid.keys()
for key in keys:
    candidateCheck(key)

final_groups = list()
for cell in cell_cand:
    for group in cell.cell_members:
        final_groups.append(group)

for group in final_groups:
    domination = 0
    for other_group in final_groups:
        if group == other_group:
            continue
        else:
            agg1 = group.aggregate
            agg2 = other_group.aggregate
            att_domination = False

            for i in range(len(agg1)):
                if agg1[i] < agg2[i]:
                    att_domination = True
                elif agg1[i] > agg2[i]:
                    att_domination = False
                    break

            if att_domination:
                domination += 1
    group.score = domination

final_groups.sort( key=lambda x: x.score, reverse=True)

if k > len(final_groups):
    outsize = len(final_groups)
else:
    outsize = k

final_rank = list()
for i in range(outsize):
    final_rank.append(final_groups[i])

time_end = datetime.datetime.now()
# total_time = time.time() - start_time
# print "Total runtime : %s seconds" % total_time

current_timestamp = time.time()
log_end_time = datetime.datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d__%H:%M:%S')

runtime = time_end - time_start

process = psutil.Process(os.getpid())
mem_usage = float(process.memory_info().rss)/1000000.0

log = list()
log_data = list()

split = filename.split('_')
aggregate_func = split[0]
group_size = split[1]
data_type = split[2]
dimension = split[3]
n_size = split[4]
combination_size = split[5]

log_data.append(log_start_time)
log_data.append(log_end_time)
log_data.append("offered algo")
log_data.append(data_type)
log_data.append(dimension)
log_data.append(n_size)
log_data.append(combination_size)
log_data.append(k)
log_data.append(group_size)
log_data.append(aggregate_func)
log_data.append(runtime)
log_data.append(mem_usage)

log.append(log_data)

with open("log.csv", "a") as output:
    writer = csv.writer(output, lineterminator='\n')
    writer.writerows(log)
    output.close()