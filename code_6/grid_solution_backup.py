import sys
import pickle
import time
import datetime
import os
import json
import csv

try:
    import psutil
    psutil_flag = True
except ImportError:
    psutil_flag = False

def keyStringToList(my_string):
    return map(int, my_string.split(','))

def keyListToString(my_list):
    return ','.join([ str(x) for x in my_list ])

current_timestamp = time.time()
log_start_time = datetime.datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d__%H:%M:%S')
time_start = datetime.datetime.now()

grid_size = 15

time1 = datetime.datetime.now()

filename = sys.argv[1]
k_size = int(sys.argv[2])
dimension = int(filename.split("_")[3])

print sys.argv

with open(filename, 'rb') as f:
    grid = pickle.load(f)
    f.close()

cell_candidates_indexes = list()
kyu = list()
visited = list()

if dimension == 2:
    kyu.append([0,0])
elif dimension == 3:
    kyu.append([0,0,0])
elif dimension == 5:
    kyu.append([0,0,0,0,0])
elif dimension == 7:
    kyu.append([0,0,0,0,0,0,0])
elif dimension == 10:
    kyu.append([0,0,0,0,0,0,0,0,0,0,])

class BreakNested(Exception):
    pass

while len(kyu) != 0:
    # print "current queue =",kyu
    current_cell_index = kyu.pop(0)
    visited.append(current_cell_index)
    current_total = 0
    #count bot left
    try:
        if dimension == 2:
            for i in range(current_cell_index[0]):
                for j in range(current_cell_index[1]):
                    temp_cell = grid[i][j]
                    current_total += temp_cell['total']
                    if current_total >= k_size:
                        raise BreakNested
        elif dimension == 3:
            for i in range(current_cell_index[0]):
                for j in range(current_cell_index[1]):
                    for k in range((current_cell_index[2])):
                        temp_cell = grid[i][j][k]
                        current_total += temp_cell['total']
                        if current_total >= k_size:
                            raise BreakNested
        elif dimension == 5:
            for i in range(current_cell_index[0]):
                for j in range(current_cell_index[1]):
                    for k in range((current_cell_index[2])):
                        for l in range(current_cell_index[3]):
                            for m in range((current_cell_index[4])):
                                temp_cell = grid[i][j][k][l][m]
                                current_total += temp_cell['total']
                                if current_total >= k_size:
                                    raise BreakNested
        elif dimension == 7:
            for i in range(current_cell_index[0]):
                for j in range(current_cell_index[1]):
                    for k in range((current_cell_index[2])):
                        for l in range(current_cell_index[3]):
                            for m in range((current_cell_index[4])):
                                for n in range(current_cell_index[5]):
                                    for o in range((current_cell_index[6])):
                                        temp_cell = grid[i][j][k][l][m][n][o]
                                        current_total += temp_cell['total']
                                        if current_total >= k_size:
                                            raise BreakNested
        elif dimension == 10:
            for i in range(current_cell_index[0]):
                for j in range(current_cell_index[1]):
                    for k in range((current_cell_index[2])):
                        for l in range(current_cell_index[3]):
                            for m in range((current_cell_index[4])):
                                for n in range(current_cell_index[5]):
                                    for o in range((current_cell_index[6])):
                                        for p in range((current_cell_index[7])):
                                            for q in range(current_cell_index[8]):
                                                for r in range((current_cell_index[9])):
                                                    temp_cell = grid[i][j][k][l][m][n][o][p][q][r]
                                                    current_total += temp_cell['total']
                                                    if current_total >= k_size:
                                                        raise BreakNested
    except BreakNested:
        pass
    # print "current total =",current_total
    if current_total < k_size:
        # print "go in cond"
        cell_candidates_indexes.append(current_cell_index)
        #enqueue next
        if dimension == 2:
            i = current_cell_index[0]
            j = current_cell_index[1]
            if i+1 < grid_size:
                x = [i+1,j]
                if x not in visited:
                    kyu.append(x)
            if j+1 < grid_size:
                x = [i,j+1]
                if x not in visited:
                    kyu.append(x)
        elif dimension == 3:
            i = current_cell_index[0]
            j = current_cell_index[1]
            k = current_cell_index[2]
            if i+1 < grid_size:
                x = [i+1,j,k]
                if x not in visited:
                    kyu.append(x)
            if j+1 < grid_size:
                x = [i,j+1,k]
                if x not in visited:
                    kyu.append(x)
            if k+1 < grid_size:
                x = [i,j,k+1]
                if x not in visited:
                    kyu.append(x)
        elif dimension == 5:
            i = current_cell_index[0]
            j = current_cell_index[1]
            k = current_cell_index[2]
            l = current_cell_index[3]
            m = current_cell_index[4]
            if i+1 < grid_size:
                x = [i+1,j,k,l,m]
                if x not in visited:
                    kyu.append(x)
            if j+1 < grid_size:
                x = [i,j+1,k,l,m]
                if x not in visited:
                    kyu.append(x)
            if k+1 < grid_size:
                x = [i,j,k+1,l,m]
                if x not in visited:
                    kyu.append(x)
            if l+1 < grid_size:
                x = [i,j,k,l+1,m]
                if x not in visited:
                    kyu.append(x)
            if m+1 < grid_size:
                x = [i,j,k,l,m+1]
                if x not in visited:
                    kyu.append(x)
        elif dimension == 7:
            i = current_cell_index[0]
            j = current_cell_index[1]
            k = current_cell_index[2]
            l = current_cell_index[3]
            m = current_cell_index[4]
            n = current_cell_index[5]
            o = current_cell_index[6]
            if i+1 < grid_size:
                x = [i+1,j,k,l,m,n,o]
                if x not in visited:
                    kyu.append(x)
            if j+1 < grid_size:
                x = [i,j+1,k,l,m,n,o]
                if x not in visited:
                    kyu.append(x)
            if k+1 < grid_size:
                x = [i,j,k+1,l,m,n,o]
                if x not in visited:
                    kyu.append(x)
            if l+1 < grid_size:
                x = [i,j,k,l+1,m,n,o]
                if x not in visited:
                    kyu.append(x)
            if m+1 < grid_size:
                x = [i,j,k,l,m+1,n,o]
                if x not in visited:
                    kyu.append(x)
            if n+1 < grid_size:
                x = [i,j,k,l,m,n+1,o]
                if x not in visited:
                    kyu.append(x)
            if o+1 < grid_size:
                x = [i,j,k,l,m,n,o+1]
                if x not in visited:
                    kyu.append(x)
        elif dimension == 10:
            i = current_cell_index[0]
            j = current_cell_index[1]
            k = current_cell_index[2]
            l = current_cell_index[3]
            m = current_cell_index[4]
            n = current_cell_index[5]
            o = current_cell_index[6]
            p = current_cell_index[7]
            q = current_cell_index[8]
            r = current_cell_index[9]
            if i+1 < grid_size:
                x = [i+1,j,k,l,m,n,o,p,q,r]
                if x not in visited:
                    kyu.append(x)
            if j+1 < grid_size:
                x = [i,j+1,k,l,m,n,o,p,q,r]
                if x not in visited:
                    kyu.append(x)
            if k+1 < grid_size:
                x = [i,j,k+1,l,m,n,o,p,q,r]
                if x not in visited:
                    kyu.append(x)
            if l+1 < grid_size:
                x = [i,j,k,l+1,m,n,o,p,q,r]
                if x not in visited:
                    kyu.append(x)
            if m+1 < grid_size:
                x = [i,j,k,l,m+1,n,o,p,q,r]
                if x not in visited:
                    kyu.append(x)
            if n+1 < grid_size:
                x = [i,j,k,l,m,n+1,o,p,q,r]
                if x not in visited:
                    kyu.append(x)
            if o+1 < grid_size:
                x = [i,j,k,l,m,n,o+1,p,q,r]
                if x not in visited:
                    kyu.append(x)
            if p+1 < grid_size:
                x = [i,j,k,l,m,n,o,p+1,q,r]
                if x not in visited:
                    kyu.append(x)
            if q+1 < grid_size:
                x = [i,j,k,l,m,n,o,p,q+1,r]
                if x not in visited:
                    kyu.append(x)
            if r+1 < grid_size:
                x = [i,j,k,l,m,n,o,p,q,r+1]
                if x not in visited:
                    kyu.append(x)

        # for i in range(len(current_cell_index)):
        #     next_cell_index = current_cell_index
        #     next_cell_index[i] = next_cell_index[i] + 1
        #     if next_cell_index[i] < grid_size:
        #         print "nebor",next_cell_index
        #         kyu.append(next_cell_index)

time2 = datetime.datetime.now()
print "candidate check done =",time2-time1
print "total cell candidates =",len(cell_candidates_indexes)

final_groups = list()
for current_index in cell_candidates_indexes:
    if dimension == 2:
        current_cell = grid[current_index[0]][current_index[1]]
    elif dimension == 3:
        current_cell = grid[current_index[0]][current_index[1]][current_index[2]]
    elif dimension == 5:
        current_cell = grid[current_index[0]][current_index[1]][current_index[2]][current_index[3]][current_index[4]]
    elif dimension == 7:
        current_cell = grid[current_index[0]][current_index[1]][current_index[2]][current_index[3]][current_index[4]][current_index[5]][current_index[6]]
    elif dimension == 10:
        current_cell = grid[current_index[0]][current_index[1]][current_index[2]][current_index[3]][current_index[4]][current_index[5]][current_index[6]][current_index[7]][current_index[8]][current_index[9]]

    for group in current_cell['cell_members']:
        final_groups.append(group)

time2set = datetime.datetime.now()
print "get group candidate from grid =", time2set-time2
print "total candidate groups =", len(final_groups)

for group_1 in final_groups:
    domination = 0
    for group_2 in final_groups:
        if group_1 == group_2:
            continue
        else:
            agg1 = group_1['aggregate']
            agg2 = group_2['aggregate']
            if agg1 == agg2:
                continue
            att_domination = False
            for i in range(len(agg1)):
                if agg1[i] < agg2[i]:
                    att_domination = True
                elif agg1[i] > agg2[i]:
                    att_domination = False
                    break
            if att_domination:
                domination += 1
    group_1['score'] = domination

time3 = datetime.datetime.now()
print "Domination score check done =",time3-time2set

final_groups.sort(key=lambda x: x['score'], reverse=True)

if k_size > len(final_groups):
    outsize = len(final_groups)
else:
    outsize = k_size

final_rank = list()
for i in range(outsize):
    final_rank.append(final_groups[i])

current_timestamp = time.time()
log_end_time = datetime.datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d__%H:%M:%S')
time_end = datetime.datetime.now()

runtime = time_end - time_start

print "total runtime =",runtime

if psutil_flag:
    process = psutil.Process(os.getpid())
    mem_usage = float(process.memory_info().rss)/1000000.0
else:
    mem_usage = "-"

log = list()
log_data = list()

split = filename.split('_')
aggregate_func = split[0]
group_size = split[1]
data_type = split[2]
# dimension = split[3]
n_size = split[4]
combination_size = split[5]

log_data.append(log_start_time)
log_data.append(log_end_time)
log_data.append("offered algo ver7")
log_data.append(data_type)
log_data.append(dimension)
log_data.append(n_size)
log_data.append(combination_size)
log_data.append(k_size)
log_data.append(group_size)
log_data.append(aggregate_func)
log_data.append(runtime)
log_data.append(mem_usage)

log.append(log_data)

#with open('json_output.json', 'w') as fout:
#   json.dump(final_rank, fout)
#   fout.close()

with open("log.csv", "a") as output:
    writer = csv.writer(output, lineterminator='\n')
    writer.writerows(log)
    output.close()
