#!/bin/bash

python initial_step.py anticorrelated_2_23.csv MAX 5
python initial_step.py anticorrelated_3_23.csv MAX 5
python initial_step.py anticorrelated_5_23.csv MAX 5

python initial_step.py anticorrelated_3_19.csv MAX 5
python initial_step.py anticorrelated_3_23.csv MAX 5
python initial_step.py anticorrelated_3_25.csv MAX 5
python initial_step.py anticorrelated_3_29.csv MAX 5
python initial_step.py anticorrelated_3_32.csv MAX 5

python initial_step.py anticorrelated_3_58.csv MAX 3
python initial_step.py anticorrelated_3_23.csv MAX 5
python initial_step.py anticorrelated_3_18.csv MAX 7
python initial_step.py anticorrelated_3_18.csv MAX 10

python initial_step.py independent_2_23.csv MAX 5
python initial_step.py independent_3_23.csv MAX 5
python initial_step.py independent_5_23.csv MAX 5

python initial_step.py independent_3_19.csv MAX 5
python initial_step.py independent_3_23.csv MAX 5
python initial_step.py independent_3_25.csv MAX 5
python initial_step.py independent_3_29.csv MAX 5
python initial_step.py independent_3_32.csv MAX 5

python initial_step.py independent_3_58.csv MAX 3
python initial_step.py independent_3_23.csv MAX 5
python initial_step.py independent_3_18.csv MAX 7
python initial_step.py independent_3_18.csv MAX 10

python initial_step.py forestcover_2_23.csv MAX 5
python initial_step.py forestcover_3_23.csv MAX 5
python initial_step.py forestcover_5_23.csv MAX 5

python initial_step.py forestcover_3_19.csv MAX 5
python initial_step.py forestcover_3_23.csv MAX 5
python initial_step.py forestcover_3_25.csv MAX 5
python initial_step.py forestcover_3_29.csv MAX 5
python initial_step.py forestcover_3_32.csv MAX 5

python initial_step.py forestcover_3_58.csv MAX 3
python initial_step.py forestcover_3_23.csv MAX 5
python initial_step.py forestcover_3_18.csv MAX 7
python initial_step.py forestcover_3_18.csv MAX 10

python initial_step.py anticorrelated_2_23.csv MIN 5
python initial_step.py anticorrelated_3_23.csv MIN 5
python initial_step.py anticorrelated_5_23.csv MIN 5

python initial_step.py anticorrelated_3_19.csv MIN 5
python initial_step.py anticorrelated_3_23.csv MIN 5
python initial_step.py anticorrelated_3_25.csv MIN 5
python initial_step.py anticorrelated_3_29.csv MIN 5
python initial_step.py anticorrelated_3_32.csv MIN 5

python initial_step.py anticorrelated_3_58.csv MIN 3
python initial_step.py anticorrelated_3_23.csv MIN 5
python initial_step.py anticorrelated_3_18.csv MIN 7
python initial_step.py anticorrelated_3_18.csv MIN 10

python initial_step.py independent_2_23.csv MIN 5
python initial_step.py independent_3_23.csv MIN 5
python initial_step.py independent_5_23.csv MIN 5

python initial_step.py independent_3_19.csv MIN 5
python initial_step.py independent_3_23.csv MIN 5
python initial_step.py independent_3_25.csv MIN 5
python initial_step.py independent_3_29.csv MIN 5
python initial_step.py independent_3_32.csv MIN 5

python initial_step.py independent_3_58.csv MIN 3
python initial_step.py independent_3_23.csv MIN 5
python initial_step.py independent_3_18.csv MIN 7
python initial_step.py independent_3_18.csv MIN 10

python initial_step.py forestcover_2_23.csv MIN 5
python initial_step.py forestcover_3_23.csv MIN 5
python initial_step.py forestcover_5_23.csv MIN 5

python initial_step.py forestcover_3_19.csv MIN 5
python initial_step.py forestcover_3_23.csv MIN 5
python initial_step.py forestcover_3_25.csv MIN 5
python initial_step.py forestcover_3_29.csv MIN 5
python initial_step.py forestcover_3_32.csv MIN 5

python initial_step.py forestcover_3_58.csv MIN 3
python initial_step.py forestcover_3_23.csv MIN 5
python initial_step.py forestcover_3_18.csv MIN 7
python initial_step.py forestcover_3_18.csv MIN 10

python initial_step.py anticorrelated_2_23.csv SUM 5
python initial_step.py anticorrelated_3_23.csv SUM 5
python initial_step.py anticorrelated_5_23.csv SUM 5

python initial_step.py anticorrelated_3_19.csv SUM 5
python initial_step.py anticorrelated_3_23.csv SUM 5
python initial_step.py anticorrelated_3_25.csv SUM 5
python initial_step.py anticorrelated_3_29.csv SUM 5
python initial_step.py anticorrelated_3_32.csv SUM 5

python initial_step.py anticorrelated_3_58.csv SUM 3
python initial_step.py anticorrelated_3_23.csv SUM 5
python initial_step.py anticorrelated_3_18.csv SUM 7
python initial_step.py anticorrelated_3_18.csv SUM 10

python initial_step.py independent_2_23.csv SUM 5
python initial_step.py independent_3_23.csv SUM 5
python initial_step.py independent_5_23.csv SUM 5

python initial_step.py independent_3_19.csv SUM 5
python initial_step.py independent_3_23.csv SUM 5
python initial_step.py independent_3_25.csv SUM 5
python initial_step.py independent_3_29.csv SUM 5
python initial_step.py independent_3_32.csv SUM 5

python initial_step.py independent_3_58.csv SUM 3
python initial_step.py independent_3_23.csv SUM 5
python initial_step.py independent_3_18.csv SUM 7
python initial_step.py independent_3_18.csv SUM 10

python initial_step.py forestcover_2_23.csv SUM 5
python initial_step.py forestcover_3_23.csv SUM 5
python initial_step.py forestcover_5_23.csv SUM 5

python initial_step.py forestcover_3_19.csv SUM 5
python initial_step.py forestcover_3_23.csv SUM 5
python initial_step.py forestcover_3_25.csv SUM 5
python initial_step.py forestcover_3_29.csv SUM 5
python initial_step.py forestcover_3_32.csv SUM 5

python initial_step.py forestcover_3_58.csv SUM 3
python initial_step.py forestcover_3_23.csv SUM 5
python initial_step.py forestcover_3_18.csv SUM 7
python initial_step.py forestcover_3_18.csv SUM 10

#python initial_step.py anticorrelated_7_23.csv MAX 5
#python initial_step.py anticorrelated_10_23.csv MAX 5
#python initial_step.py independent_7_23.csv MAX 5
#python initial_step.py independent_10_23.csv MAX 5
#python initial_step.py forestcover_7_23.csv MAX 5
#python initial_step.py forestcover_10_23.csv MAX 5

#python initial_step.py anticorrelated_7_23.csv MIN 5
#python initial_step.py anticorrelated_10_23.csv MIN 5
#python initial_step.py independent_7_23.csv MIN 5
#python initial_step.py independent_10_23.csv MIN 5
#python initial_step.py forestcover_7_23.csv MIN 5
#python initial_step.py forestcover_10_23.csv MIN 5

#python initial_step.py anticorrelated_7_23.csv SUM 5
#python initial_step.py anticorrelated_10_23.csv SUM 5
#python initial_step.py independent_7_23.csv SUM 5
#python initial_step.py independent_10_23.csv SUM 5
#python initial_step.py forestcover_7_23.csv SUM 5
#python initial_step.py forestcover_10_23.csv SUM 5

echo "script initial step done"

