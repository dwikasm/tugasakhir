#!/bin/bash

#grid solution (filename: aggfunc_g_anticorrelated_d_n_comb, k)

#1000k
python grid_solution.py max_5_anticorrelated_3_23_33649 1000
python grid_solution.py max_5_independent_3_23_33649 1000
python grid_solution.py max_5_forestcover_3_23_33649 1000

#5dimensi
python grid_solution.py max_5_anticorrelated_5_23_33649 200
python grid_solution.py max_5_independent_5_23_33649 200
python grid_solution.py max_5_forestcover_5_23_33649 200

echo "script solution pelengkap max done"
