import sys
import pickle
import time
import datetime
import itertools
import os
import json
import csv

try:
    import psutil
    psutil_flag = True
except ImportError:
    psutil_flag = False


class Group:
    def __init__(self):
        self.score = 0
        self.group_members = []
        self.aggregate = []


class GridCell:
    def __init__(self):
        self.total = 0
        self.cell_members = []
        self.position = ""

    def add_cell_member(self, my_group):
        self.cell_members.append(my_group)
        self.total += 1


def keyStringToList(my_string):
    return map(int, my_string.split(','))


def keyListToString(my_list):
    return ','.join([ str(x) for x in my_list ])


def myToJson(my_list_of_groups):
    list_of_dict = list()
    for group in my_list_of_groups:
        list_of_dict.append({
            "group_members" : str(group.group_members),
            "aggregate" : str(group.aggregate),
            "score" : group.score
        })
    return list_of_dict


def candidateCheck(curpos_string):
    global grid
    global cell_cand
    global cell_not_cand
    global k

    # print curpos_string
    # print "cell cand", cell_cand.cell_member
    curpos = keyStringToList(curpos_string)

    list_of_list = list()
    for x in curpos:
        lis = list()
        for y in range(1, x+1):
            lis.append(y)
        list_of_list.append(lis)

    products = list(itertools.product(*list_of_list))
    better_cell = list()
    partial_cell = list()
    maximum_size = 0
    candidate_flag = True

    # print "curpos", curpos
    # print "curpos string",curpos_string

    # Create all possible cell which is smaller than current position
    for product in products:
        lproduct = list(product)
        att_diff = 0
        for i in range(len(lproduct)):
            if lproduct[i] != curpos[i]:
                att_diff += 1
        # print curpos,"lproduct",lproduct,att_diff
        if att_diff == len(lproduct):
            better_cell.append(lproduct)
        elif att_diff > 0:
            partial_cell.append(lproduct)

    # for x in better_cell:
    #     print "x",keyListToString(x),x,grid.keys()
    # print curpos_string

    # for cell in partial_cell:
    #     cell_key = keyListToString(cell)
    #     if cell_key in grid.keys():
    #         if grid[cell_key] in cell_not_cand:
    #             cell_not_cand.append(grid[curpos_string])
    #             return

    # print "grid.keys()", grid.keys()
    # print "grid.keys()[0]",grid.keys()[0]
    # print "curpos_string",curpos_string
    # print "curpos_string == grid.keys()[0]",curpos_string == grid.keys()[0]
    # if curpos_string in grid.keys():
    #     print "YES"
    # else:
    #     print "NO"
    # sys.exit()

    # for cell in partial_cell:
    #     cell_key = keyListToString(cell)
    #     if cell_key in grid.keys():
    #         if grid[cell_key] in cell_not_cand:
    #             candidate_flag = False
    #             break

    if candidate_flag:
        for cell in better_cell:
            cell_key = keyListToString(cell)
            # print "\ncell_key!!!",cell_key
            # print "grid keys", grid.keys()
            # for x in grid.keys():
            #     print "x =",x
            #     print "typex =", type(x)
            #     print "cell_key =",cell_key
            #     print "type cell_key =", type(cell_key)
            #     print "cell_key == x",cell_key == x
            # sys.exit()
            if cell_key in grid.keys():
                # print "MASUK"
                # print "cell key", cell_key
                if grid[cell_key] in cell_not_cand:
                    # cell_not_cand.append(grid[curpos_string])
                    # return
                    candidate_flag = False
                    break
                maximum_size += grid[cell_key].total
                # print maximum_size
            # else:
            #     print "gamasuk"
            # print "maximum_size =", maximum_size, "k =", k
            if maximum_size > k:
                candidate_flag = False
                break

    # sys.exit()

    if candidate_flag:
        cell_cand.append(grid[curpos_string])
    else:
        cell_not_cand.append(grid[curpos_string])
    return

current_timestamp = time.time()
log_start_time = datetime.datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d__%H:%M:%S')
time_start = datetime.datetime.now()

filename = sys.argv[1]
k = int(sys.argv[2])

with open(filename, 'rb') as f:
    grid = pickle.load(f)
    f.close()

time1 = datetime.datetime.now()

cell_cand = list()
cell_not_cand = list()

keys = grid.keys()

# for key in keys:
#     print key, ":",grid[key].total

# print "keys",keys

for key in keys:
    candidateCheck(key)

time2 = datetime.datetime.now()

final_groups = list()
for cell in cell_cand:
    for group in cell.cell_members:
        final_groups.append(group)

time3 = datetime.datetime.now()
print "all group candidate", len(final_groups)
# sys.exit()

komparasi = 0
for group in final_groups:
    domination = 0
    for other_group in final_groups:
        komparasi += 1
        # print group.aggregate,"vs",other_group.aggregate,"at",datetime.datetime.now()
        if group == other_group:
            continue
        else:
            agg1 = group.aggregate
            agg2 = other_group.aggregate
            att_domination = False
            for i in range(len(agg1)):
                if agg1[i] < agg2[i]:
                    att_domination = True
                elif agg1[i] > agg2[i]:
                    att_domination = False
                    break
            if att_domination:
                domination += 1
    group.score = domination
print "komparasi =", komparasi
time4 = datetime.datetime.now()

final_groups.sort(key=lambda x: x.score, reverse=True)

if k > len(final_groups):
    outsize = len(final_groups)
else:
    outsize = k

final_rank = list()
for i in range(outsize):
    final_rank.append(final_groups[i])

time_end = datetime.datetime.now()
# total_time = time.time() - start_time
# print "Total runtime : %s seconds" % total_time

current_timestamp = time.time()
log_end_time = datetime.datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d__%H:%M:%S')

runtime = time_end - time_start

if psutil_flag:
    process = psutil.Process(os.getpid())
    mem_usage = float(process.memory_info().rss)/1000000.0
else:
    mem_usage = "-"

print "Candidate check =", time2-time1
print "Append all group candidate =", time3-time2
print "Group domination check =", time4-time3

log = list()
log_data = list()

split = filename.split('_')
aggregate_func = split[0]
group_size = split[1]
data_type = split[2]
dimension = split[3]
n_size = split[4]
combination_size = split[5]

log_data.append(log_start_time)
log_data.append(log_end_time)
log_data.append("offered algo ver2")
log_data.append(data_type)
log_data.append(dimension)
log_data.append(n_size)
log_data.append(combination_size)
log_data.append(k)
log_data.append(group_size)
log_data.append(aggregate_func)
log_data.append(runtime)
log_data.append(mem_usage)

log.append(log_data)

# python grid_solution.py max_5_anticorrelated_3_19_11628 200
print "runtime :",runtime
# sys.exit()
# for x in final_rank:
#     print x.group_members,":",x.aggregate
# sys.exit()

#list_of_dict = myToJson(final_rank)
# print output_json

#with open('json_output', 'w') as fout:
#    json.dump(list_of_dict, fout)
#    fout.close()
#sys.exit()

with open("log.csv", "a") as output:
    writer = csv.writer(output, lineterminator='\n')
    writer.writerows(log)
    output.close()
