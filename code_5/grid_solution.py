import sys
import pickle
import time
import datetime
import os
import json
import csv

try:
    import psutil
    psutil_flag = True
except ImportError:
    psutil_flag = False

def keyStringToList(my_string):
    return map(int, my_string.split(','))

def keyListToString(my_list):
    return ','.join([ str(x) for x in my_list ])

def notCandidateFlagging(current_cell_key, dimension):
    global grid
    print "flagging"
    if dimension == '2':
        for i in range(current_cell_key[0], 26):
            for j in range(current_cell_key[1], 26):
                key_string = keyListToString([i, j])
                if key_string in grid.keys():
                    current_cell = grid[key_string]
                    current_cell['is_not_cand'] = 1
    elif dimension == '3':
        for i in range(current_cell_key[0], 26):
            for j in range(current_cell_key[1], 26):
                for k in range(current_cell_key[2], 26):
                    key_string = keyListToString([i, j, k])
                    if key_string in grid.keys():
                        current_cell = grid[key_string]
                        current_cell['is_not_cand'] = 1
    elif dimension == '5':
        for i in range(current_cell_key[0], 26):
            for j in range(current_cell_key[1], 26):
                for k in range(current_cell_key[2], 26):
                    for l in range(current_cell_key[3], 26):
                        for m in range(current_cell_key[4], 26):
                            key_string = keyListToString([i, j, k, l, m])
                            if key_string in grid.keys():
                                current_cell = grid[key_string]
                                current_cell['is_not_cand'] = 1
    elif dimension == '7':
        for i in range(current_cell_key[0], 26):
            for j in range(current_cell_key[1], 26):
                for k in range(current_cell_key[2], 26):
                    for l in range(current_cell_key[3], 26):
                        for m in range(current_cell_key[4], 26):
                            for n in range(current_cell_key[5], 26):
                                for o in range(current_cell_key[6], 26):
                                    key_string = keyListToString([i, j, k, l, m, n, o])
                                    if key_string in grid.keys():
                                        current_cell = grid[key_string]
                                        current_cell['is_not_cand'] = 1
    elif dimension == '10':
        for i in range(current_cell_key[0], 26):
            for j in range(current_cell_key[1], 26):
                for k in range(current_cell_key[2], 26):
                    for l in range(current_cell_key[3], 26):
                        for m in range(current_cell_key[4], 26):
                            for n in range(current_cell_key[5], 26):
                                for o in range(current_cell_key[6], 26):
                                    for p in range(current_cell_key[7], 26):
                                        for q in range(current_cell_key[8], 26):
                                            for r in range(current_cell_key[9], 26):
                                                key_string = keyListToString([i, j, k, l, m, n, o, p, q, r])
                                                if key_string in grid.keys():
                                                    current_cell = grid[key_string]
                                                    current_cell['is_not_cand'] = 1

    return

def candidateCheck(curpos_string, dimension):
    global grid
    global cell_cand_list
    global cell_not_cand_list
    global k

    print "checking ", curpos_string
    curpos = keyStringToList(curpos_string)
    candidate_flag = True
    number_of_dominator = 0
    print "test"

    class BreakNested(Exception):
        pass

    if dimension == '2':
        try:
            for i in range(1, curpos[0]+1):
                for j in range(1, curpos[1]+1):
                    current_cell_key = [i, j]
                    current_cell_key_s = keyListToString(current_cell_key)

                    if current_cell_key_s in grid.keys():
                        current_cell = grid[current_cell_key_s]
                        # sel bukan kandidat jika sel yang mendominasi dia adalah anggota sel bukan kandidat
                        if current_cell['is_not_cand'] != 0:
                            notCandidateFlagging(current_cell_key_s, dimension)
                            candidate_flag = False
                            pass
                        if current_cell in cell_not_cand_list:
                            candidate_flag = False
                            raise BreakNested
                        else:
                            number_of_dominator += current_cell['total']
                    # sel bukan kandidat jika jumlah yang mendominasi dia lebih dari k
                    if number_of_dominator > k:
                        candidate_flag = False
                        raise BreakNested
        except BreakNested:
            pass

        if candidate_flag:
            cell_cand_list.append(grid[curpos_string])
        else:
            current_cell = grid[curpos_string]
            notCandidateFlagging(curpos, dimension)
            current_cell['is_not_cand'] = 1
            cell_not_cand_list.append(current_cell)
    
    elif dimension == '3':
        try:
            for i in range(1, curpos[0]+1):
                for j in range(1, curpos[1]+1):
                    for k in range(1, curpos[2]+1):
                        current_cell_key = [i, j, k]
                        current_cell_key_s = keyListToString(current_cell_key)

                        if current_cell_key_s in grid.keys():
                            current_cell = grid[current_cell_key_s]
                            # sel bukan kandidat jika sel yang mendominasi dia adalah anggota sel bukan kandidat
                            if current_cell['is_not_cand'] != 0:
                                notCandidateFlagging(current_cell_key_s, dimension)
                                candidate_flag = False
                                pass
                            if current_cell in cell_not_cand_list:
                                candidate_flag = False
                                raise BreakNested
                            else:
                                number_of_dominator += current_cell['total']
                        # sel bukan kandidat jika jumlah yang mendominasi dia lebih dari k
                        if number_of_dominator > k:
                            candidate_flag = False
                            raise BreakNested
        except BreakNested:
            pass

        if candidate_flag:
            cell_cand_list.append(grid[curpos_string])
        else:
            current_cell = grid[curpos_string]
            notCandidateFlagging(curpos, dimension)
            current_cell['is_not_cand'] = 1
            cell_not_cand_list.append(current_cell)
    
    elif dimension == '5':
        try:
            for i in range(1, curpos[0]+1):
                for j in range(1, curpos[1]+1):
                    for k in range(1, curpos[2]+1):
                        for l in range(1, curpos[3]+1):
                            for m in range(1, curpos[4]+1):
                                current_cell_key = [i, j, k, l, m]
                                current_cell_key_s = keyListToString(current_cell_key)
                                if current_cell_key_s in grid.keys():
                                    current_cell = grid[current_cell_key_s]
                                    # sel bukan kandidat jika sel yang mendominasi dia adalah anggota sel bukan kandidat
                                    if current_cell['is_not_cand'] != 0:
                                        notCandidateFlagging(current_cell_key_s, dimension)
                                        candidate_flag = False
                                        pass
                                    if current_cell in cell_not_cand_list:
                                        candidate_flag = False
                                        raise BreakNested
                                    else:
                                        number_of_dominator += current_cell['total']
                                # sel bukan kandidat jika jumlah yang mendominasi dia lebih dari k
                                if number_of_dominator > k:
                                    candidate_flag = False
                                    raise BreakNested
        except BreakNested:
            pass

        if candidate_flag:
            cell_cand_list.append(grid[curpos_string])
        else:
            current_cell = grid[curpos_string]
            notCandidateFlagging(curpos, dimension)
            current_cell['is_not_cand'] = 1
            cell_not_cand_list.append(current_cell)
    
    elif dimension == '7':
        try:
            for i in range(1, curpos[0]+1):
                for j in range(1, curpos[1]+1):
                    for k in range(1, curpos[2]+1):
                        for l in range(1, curpos[3]+1):
                            for m in range(1, curpos[4]+1):
                                for n in range(1, curpos[5]+1):
                                    for o in range(1, curpos[6]+1):
                                        current_cell_key = [i, j, k, l, m, n, o]
                                        current_cell_key_s = keyListToString(current_cell_key)
                                        # print curpos_string,":",current_cell_key
                                        if current_cell_key_s in grid.keys():
                                            print current_cell_key_s
                                            # sys.exit()
                                            current_cell = grid[current_cell_key_s]
                                            # sel bukan kandidat jika sel yang mendominasi dia adalah anggota sel bukan kandidat
                                            if current_cell['is_not_cand'] != 0:
                                                notCandidateFlagging(current_cell_key_s, dimension)
                                                candidate_flag = False
                                                pass
                                            if current_cell in cell_not_cand_list:
                                                candidate_flag = False
                                                raise BreakNested
                                            else:
                                                number_of_dominator += current_cell['total']
                                        #sel bukan kandidat jika jumlah yang mendominasi dia lebih dari k
                                        if number_of_dominator > k:
                                            candidate_flag = False
                                            raise BreakNested
        except BreakNested:
            pass
        
        if candidate_flag:
            cell_cand_list.append(grid[curpos_string])
        else:
            current_cell = grid[curpos_string]
            notCandidateFlagging(curpos, dimension)
            current_cell['is_not_cand'] = 1
            cell_not_cand_list.append(current_cell)

            
    elif dimension == '10':
        try:
            for i in range(1, curpos[0]+1):
                for j in range(1, curpos[1]+1):
                    for k in range(1, curpos[2]+1):
                        for l in range(1, curpos[3]+1):
                            for m in range(1, curpos[4]+1):
                                for n in range(1, curpos[5]+1):
                                    for o in range(1, curpos[6]+1):
                                        for p in range(1, curpos[7]+1):
                                            for q in range(1, curpos[8]+1):
                                                for r in range(1, curpos[9]+1):
                                                    current_cell_key = [i, j, k, l, m, n, o, p, q, r]
                                                    current_cell_key_s = keyListToString(current_cell_key)
                                                    if current_cell_key_s in grid.keys():
                                                        current_cell = grid[current_cell_key_s]
                                                        # sel bukan kandidat jika sel yang mendominasi dia adalah anggota sel bukan kandidat
                                                        if current_cell['is_not_cand'] != 0:
                                                            notCandidateFlagging(current_cell_key_s, dimension)
                                                            candidate_flag = False
                                                            pass
                                                        if current_cell in cell_not_cand_list:
                                                            candidate_flag = False
                                                            raise BreakNested
                                                        else:
                                                            number_of_dominator += current_cell['total']
                                                    # sel bukan kandidat jika jumlah yang mendominasi dia lebih dari k
                                                    if number_of_dominator > k:
                                                        candidate_flag = False
                                                        raise BreakNested
        except BreakNested:
            pass

        if candidate_flag:
            cell_cand_list.append(grid[curpos_string])
        else:
            current_cell = grid[curpos_string]
            notCandidateFlagging(curpos, dimension)
            current_cell['is_not_cand'] = 1
            cell_not_cand_list.append(current_cell)

    return

current_timestamp = time.time()
log_start_time = datetime.datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d__%H:%M:%S')
time_start = datetime.datetime.now()

filename = sys.argv[1]
k = int(sys.argv[2])
dimension = filename.split("_")[3]

with open(filename, 'rb') as f:
    grid = pickle.load(f)
    f.close()

time1 = datetime.datetime.now()

cell_cand_list = list()
cell_not_cand_list = list()

keys = sorted(grid.keys())
for key in keys:
    candidateCheck(key,dimension)

time2 = datetime.datetime.now()

final_groups = list()

for cell in cell_cand_list:
    for group in cell['cell_members']:
        final_groups.append(group)

time3 = datetime.datetime.now()
print "all group candidate", len(final_groups)

komparasi = 0
for group in final_groups:
    domination = 0
    for other_group in final_groups:
        komparasi += 1
        if group == other_group:
            continue
        else:
            agg1 = group['aggregate']
            agg2 = other_group['aggregate']
            att_domination = False
            for i in range(len(agg1)):
                if agg1[i] < agg2[i]:
                    att_domination = True
                elif agg1[i] > agg2[i]:
                    att_domination = False
                    break
            if att_domination:
                domination += 1
    group['score'] = domination

print "komparasi =", komparasi
time4 = datetime.datetime.now()

final_groups.sort(key=lambda x: x['score'], reverse=True)

if k > len(final_groups):
    outsize = len(final_groups)
else:
    outsize = k

final_rank = list()
for i in range(outsize):
    final_rank.append(final_groups[i])

time_end = datetime.datetime.now()

current_timestamp = time.time()
log_end_time = datetime.datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d__%H:%M:%S')

runtime = time_end - time_start

if psutil_flag:
    process = psutil.Process(os.getpid())
    mem_usage = float(process.memory_info().rss)/1000000.0
else:
    mem_usage = "-"

print "Candidate check =", time2-time1
print "Append all group candidate =", time3-time2
print "Group domination check =", time4-time3

log = list()
log_data = list()

split = filename.split('_')
aggregate_func = split[0]
group_size = split[1]
data_type = split[2]
# dimension = split[3]
n_size = split[4]
combination_size = split[5]

log_data.append(log_start_time)
log_data.append(log_end_time)
log_data.append("offered algo ver4")
log_data.append(data_type)
log_data.append(dimension)
log_data.append(n_size)
log_data.append(combination_size)
log_data.append(k)
log_data.append(group_size)
log_data.append(aggregate_func)
log_data.append(runtime)
log_data.append(mem_usage)

log.append(log_data)

# python grid_solution.py max_5_anticorrelated_3_19_11628 200
print "runtime :", runtime
# sys.exit()
# for x in final_rank:
#     print x.group_members,":",x.aggregate
# sys.exit()

list_of_dict = final_rank
# print output_json

with open('json_output.json', 'w') as fout:
   json.dump(list_of_dict, fout)
   fout.close()

with open("log.csv", "a") as output:
    writer = csv.writer(output, lineterminator='\n')
    writer.writerows(log)
    output.close()
