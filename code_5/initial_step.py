import sys
import csv
import itertools
import math
import pickle
import time
import datetime

def keyStringToList(my_string):
    return map(int, my_string.split(','))

def keyListToString(my_list):
    return ','.join( [ str(x) for x in my_list ] )

print "running : "+str(sys.argv[0])+", "+str(sys.argv[1])+", "+str(sys.argv[2])+", "+str(sys.argv[3])
filename = sys.argv[1]
aggfunc = (sys.argv[2]).upper()
group_size = int(sys.argv[3])

with open(filename, 'rb') as f:
    reader = csv.reader(f)
    header = next(reader)
    rows = list()
    for row in reader:
        rowInt = list()
        for i in range(len(row)):
            rowInt.append(int(row[i]))
        rows.append(rowInt)
    f.close()

att_size = len(header)
combinations = list(itertools.combinations(rows, group_size))
group_list = list()
max_agg = -sys.maxint

for combination in combinations:
    if aggfunc == 'MAX':
        att = [-sys.maxint] * att_size
    elif aggfunc == 'MIN':
        att = [sys.maxint] * att_size
    elif aggfunc == 'SUM':
        att = [0] * att_size
    else:
        print "aggregate function error"
        sys.exit(0)

    for tuple in combination:
        for i in range(att_size):
            if aggfunc == 'MAX':
                att[i] = max(int(att[i]), int(tuple[i]))
            elif aggfunc == 'MIN':
                att[i] = min(int(att[i]), int(tuple[i]))
            elif aggfunc == 'SUM':
                att[i] = int(att[i]) + int(tuple[i])

    aggregate = list()
    for i in range(att_size):
        aggregate.append(att[i])
        max_agg = max(max_agg, att[i])

    new_group = {
        'group_members' : combination,
        'aggregate' : aggregate
    }

    group_list.append(new_group)

grid = dict()
# grid_distance = int( math.ceil( float(max_agg)/25 ) )
grid_distance = int( math.ceil( float(max_agg)/15 ) )

for data in group_list:
    position_by_grid_distance = []
    for aggregate_att in data['aggregate']:
        position_by_grid_distance.append( int( math.ceil( float(aggregate_att)/grid_distance ) ) )

    grid_position = keyListToString(position_by_grid_distance)

    if grid.has_key(grid_position):
        cell = grid[grid_position]
        cell['total'] = cell['total'] + 1
        cell['cell_members'].append(data)

        grid.update( { grid_position : cell } )
    else:
        # new_cell = GridCell()
        # new_cell.add_cell_member(data)
        # new_cell.position = grid_position
        new_cell_members = list()
        new_cell_members.append(data)
        new_cell = {
            'total' : 1,
            'cell_members' : new_cell_members,
            'position' : grid_position,
            'is_not_cand' : 0
        }

        grid.update( { grid_position : new_cell } )

outfilename = aggfunc.lower() + "_" + str(group_size) + "_" + filename.split('.csv')[0] + "_" + str(len(combinations))
with open(outfilename, 'wb') as fo:
    pickle.dump(grid, fo)
    fo.close()

print "OK,",outfilename,",",datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d__%H:%M:%S')