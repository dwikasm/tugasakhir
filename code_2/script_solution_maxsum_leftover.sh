#!/bin/bash

#grid solution (filename: aggfunc_g_anticorrelated_d_n_comb, k)

python grid_solution.py max_5_anticorrelated_7_23_33649 200
python grid_solution.py max_5_anticorrelated_10_23_33649 200
python grid_solution.py max_5_independent_7_23_33649 200
python grid_solution.py max_5_independent_10_23_33649 200
python grid_solution.py max_5_forestcover_7_23_33649 200
python grid_solution.py max_5_forestcover_10_23_33649 200

python grid_solution.py sum_5_anticorrelated_7_23_33649 200
python grid_solution.py sum_5_anticorrelated_10_23_33649 200
python grid_solution.py sum_5_independent_7_23_33649 200
python grid_solution.py sum_5_independent_10_23_33649 200
python grid_solution.py sum_5_forestcover_7_23_33649 200
python grid_solution.py sum_5_forestcover_10_23_33649 200

echo "script solution maxsum leftover done"
