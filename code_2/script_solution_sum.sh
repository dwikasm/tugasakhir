#!/bin/bash

#grid solution (filename: aggfunc_g_anticorrelated_d_n_comb, k)

python grid_solution.py sum_5_anticorrelated_2_23_33649 200
python grid_solution.py sum_5_anticorrelated_3_23_33649 200
python grid_solution.py sum_5_anticorrelated_5_23_33649 200



python grid_solution.py sum_5_anticorrelated_3_19_11628 200
python grid_solution.py sum_5_anticorrelated_3_23_33649 200
python grid_solution.py sum_5_anticorrelated_3_25_53130 200



python grid_solution.py sum_5_anticorrelated_3_23_33649 100
python grid_solution.py sum_5_anticorrelated_3_23_33649 200
python grid_solution.py sum_5_anticorrelated_3_23_33649 500


python grid_solution.py sum_3_anticorrelated_3_58_30856 200
python grid_solution.py sum_5_anticorrelated_3_23_33649 200
python grid_solution.py sum_7_anticorrelated_3_18_31824 200


python grid_solution.py sum_5_independent_2_23_33649 200
python grid_solution.py sum_5_independent_3_23_33649 200
python grid_solution.py sum_5_independent_5_23_33649 200



python grid_solution.py sum_5_independent_3_19_11628 200
python grid_solution.py sum_5_independent_3_23_33649 200
python grid_solution.py sum_5_independent_3_25_53130 200



python grid_solution.py sum_5_independent_3_23_33649 100
python grid_solution.py sum_5_independent_3_23_33649 200
python grid_solution.py sum_5_independent_3_23_33649 500


python grid_solution.py sum_3_independent_3_58_30856 200
python grid_solution.py sum_5_independent_3_23_33649 200
python grid_solution.py sum_7_independent_3_18_31824 200


python grid_solution.py sum_5_forestcover_2_23_33649 200
python grid_solution.py sum_5_forestcover_3_23_33649 200
python grid_solution.py sum_5_forestcover_5_23_33649 200



python grid_solution.py sum_5_forestcover_3_19_11628 200
python grid_solution.py sum_5_forestcover_3_23_33649 200
python grid_solution.py sum_5_forestcover_3_25_53130 200



python grid_solution.py sum_5_forestcover_3_23_33649 100
python grid_solution.py sum_5_forestcover_3_23_33649 200
python grid_solution.py sum_5_forestcover_3_23_33649 500


python grid_solution.py sum_3_forestcover_3_58_30856 200
python grid_solution.py sum_5_forestcover_3_23_33649 200
python grid_solution.py sum_7_forestcover_3_18_31824 200


python grid_solution.py sum_5_anticorrelated_7_23_33649 200
python grid_solution.py sum_5_anticorrelated_10_23_33649 200

python grid_solution.py sum_5_anticorrelated_3_29_118755 200
python grid_solution.py sum_5_anticorrelated_3_32_201376 200

python grid_solution.py sum_5_anticorrelated_3_23_33649 100

python grid_solution.py sum_10_anticorrelated_3_18_43758 200

python grid_solution.py sum_5_independent_7_23_33649 200
python grid_solution.py sum_5_independent_10_23_33649 200

python grid_solution.py sum_5_independent_3_29_118755 200
python grid_solution.py sum_5_independent_3_32_201376 200

python grid_solution.py sum_5_independent_3_23_33649 100

python grid_solution.py sum_10_independent_3_18_43758 200

python grid_solution.py sum_5_forestcover_7_23_33649 200
python grid_solution.py sum_5_forestcover_10_23_33649 200

python grid_solution.py sum_5_forestcover_3_29_118755 200
python grid_solution.py sum_5_forestcover_3_32_201376 200

python grid_solution.py sum_5_forestcover_3_23_33649 100

python grid_solution.py sum_10_forestcover_3_18_43758 200

echo "script solution sum done"